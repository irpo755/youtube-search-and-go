# React Native Youtube Search and Go App

*Search and go is a react native app for search and watch youtube videos*

## Installation

**Step 1:** clone my repo & cd into project

**Step 2:** install node modules

```
yarn install
```

*Before run android build, setup [Android Studio](https://facebook.github.io/react-native/docs/android-setup.html)*

**Step 3:** You'll need react-native cli
```
yarn add -g react-native-cli
```

**Step 4:** Create a .env file with your Youtube API Key
```
YOUTUBE_API_KEY=YOURAPIKEY
```

**Step 5:** If Android

```
react-native run-android
```

*Before running iOS build, Install [Xcode](https://developer.apple.com/xcode/download/)*

**Step 6:** If iOS

```
react-native run-ios
```

That's all

If you want to install app in the device, connect your device to the system with debugger mode on and run the above command (app will be installed automatically)