import Config from 'react-native-config'

export const YOUTUBE_API_URL = 'https://www.googleapis.com/youtube/v3'

export const YOUTUBE_API_KEY = Config.YOUTUBE_API_KEY