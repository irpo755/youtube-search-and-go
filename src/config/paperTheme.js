export default (DefaultTheme) => {
    return {
        ...DefaultTheme,
        roundness: 0,
        colors: {
            ...DefaultTheme.colors,
            primary: '#FF0000',
            accent: '#065FD4',
            text: '#0D0D0D'
        }
    }
}