import {NavigationActions} from 'react-navigation'

export const navigateToSplash = () =>
  NavigationActions.navigate({
    routeName: 'Splash',
})

export const navigateToSearchVideos = () =>
    NavigationActions.navigate({
        routeName: 'SearchVideos',
    })


export const navigateToVideo = () => 
    NavigationActions.navigate({
        routeName: 'VideoPlayer',
    })