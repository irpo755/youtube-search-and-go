import React, { Component } from "react"
import { connect } from "react-redux"
import { addNavigationHelpers, createAppContainer } from "react-navigation"
import NavigationStack from "./navigationStack"

const mapStateToProps = state => {
  return {
    nav: state.nav
  }
}

export default connect(mapStateToProps)(createAppContainer(NavigationStack))