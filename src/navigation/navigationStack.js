import { createStackNavigator } from 'react-navigation-stack';
import SearchScreen from '../features/searchVideos/container'
import VideoPlayer from '../features/videoPlayer/container'
import SplashScreen from '../features/splashScreen/components/SplashScreen'

export default createStackNavigator(
    {
        VideoPlayer: {
            screen: VideoPlayer
        },
        SearchVideos: {
            screen: SearchScreen
        },
        SplashScreen:{
            screen: SplashScreen
        }
    },
    {
        initialRouteName: 'SearchVideos'
    }
)