import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    activityIndicatorContainer: {
        height: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    chipStyle:{
        backgroundColor: 'white',
        alignContent: 'flex-end'
    },
    rowReverseContainer: {
        flex: 1, flexDirection: 'row-reverse'
    }
})