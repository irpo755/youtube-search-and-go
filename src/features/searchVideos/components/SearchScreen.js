import React from 'react'
import {ScrollView, View} from 'react-native'
import { ActivityIndicator } from 'react-native-paper'
import MyAppBar from './MyAppBar'
import VideoCard from './VideoCard'
import styles from 'app/styles'

class SearchScreen extends React.Component {
    static navigationOptions = (props) => ({
        header: (
            <MyAppBar triggerSearch={props.navigation.state.params ? props.navigation.state.params.triggerSearch : () => {}}/>
        )
    })

    componentDidMount() {
        this.props.navigation.setParams({
            triggerSearch: this.props.triggerSearch
        })
    }

    render() {
        const {videos, isSearching} = this.props
        return isSearching ? 
            (
                <View style={styles.activityIndicatorContainer}>
                    <ActivityIndicator animating={true} />
                </View>
            )
            :
            (
            <ScrollView>
                {videos.map((video) => 
                    <VideoCard
                    key={video.id}
                    description={video.description}
                    title={video.title}
                    image={video.thumbnail}
                    goToVideo={() => {this.props.goToVideo(video, this.props.navigation)}}
                    />
                )}             
            </ScrollView>
        )
    }
}

export default SearchScreen

