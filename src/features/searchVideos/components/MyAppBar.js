import React from 'react'
import DeviceHour from 'app/components/DeviceHour'
import { View } from 'react-native'
import { Appbar , Searchbar  } from 'react-native-paper'

class MyAppBar extends React.Component {

    onChangeText = (lastValue) => {
        this.props.triggerSearch(lastValue)
    }

    render(){
        return (
        <View>
            <Appbar.Header>
                <Appbar.Content 
                    title="Youtube Search&Go"
                />
                <DeviceHour/>
            </Appbar.Header>
            <Searchbar 
                placeholder="Search"
                onChangeText={this.onChangeText}
            />
        </View>
        )
    }
}

export default MyAppBar