import React from 'react'
import { Card, Title, Paragraph, TouchableRipple } from 'react-native-paper'

const VideoCard = ({image, description, title, goToVideo}) => (
    <TouchableRipple rippleColor="#DDE3E6">
        <Card onPress={ () => { goToVideo() }}>
        <Card.Cover source={{uri: image}} />
        <Card.Content>
            <Title>{title}</Title>
            <Paragraph>{ description.length > 125 ? description.substring(0,124) + "..." : description }</Paragraph>
        </Card.Content>
        </Card>
    </TouchableRipple>
)

export default VideoCard