import { call, put, debounce, all } from 'redux-saga/effects'
import { SEARCH_VIDEOS, SET_RESULTS_VIDEOS, SEARCH_FAILED, START_SEARCHING, STOP_SEARCHING } from './constants'
import * as ytapi  from 'app/api/youtube'

function* searchVideos(action) {
    try{
        yield put({type: START_SEARCHING})
        const videos = yield call(ytapi.searchVideos, action.payload)
        const formattedVideos = videos.data.items.map((video) => {
            return {
                id: video.id.videoId,
                channelId: video.snippet.channelId,
                channelTitle: video.snippet.channelTitle,
                description: video.snippet.description,
                title: video.snippet.title,
                thumbnail: video.snippet.thumbnails.high.url
            }
        })
        yield put({type: STOP_SEARCHING})
        yield put({type: SET_RESULTS_VIDEOS, payload: formattedVideos})
    }catch(error){
        console.log(SEARCH_FAILED, error)
    }
}


function* searchVideosSaga(){
    yield debounce(500,SEARCH_VIDEOS, searchVideos)
}

export default searchVideosSaga