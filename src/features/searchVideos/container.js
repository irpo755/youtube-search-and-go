import { connect } from 'react-redux'
import {searchVideos, setSelectedVideo} from './actions'
import {navigateToVideo} from 'app/navigation/actions'
import SearchScreen from './components/SearchScreen'

const triggerSearch = (query, dispatch) => {
    dispatch(searchVideos(query))
}

const dispatchSelectedVideo = (video, dispatch) => {
    dispatch(setSelectedVideo(video))
}

mapStateToProps = (state) => ({
    videos: state.SearchVideos.resultVideos,
    isSearching: state.SearchVideos.isSearching
})

const mapDispatchToProps = (dispatch) =>{
    return {
        triggerSearch: (query) => {
            triggerSearch(query, dispatch)
        },
        goToVideo: (video, navigation) => {
            dispatchSelectedVideo(video, dispatch)
            //should be .dispatch(navigateToVideo()) but with persistance this stop working
            navigation.dispatch(navigateToVideo())
        }

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)