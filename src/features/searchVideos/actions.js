import {SEARCH_VIDEOS, SET_SELECTED_VIDEO} from './constants'

export const searchVideos = (query) => ({
        type: SEARCH_VIDEOS,
        payload: query
})

export const setSelectedVideo = video => ({
        type: SET_SELECTED_VIDEO,
        payload: video
})