import {
    FETCH_VIDEO_DETAILS,
    CLEAN_VIDEO,
    FETCH_COMMENTS
} from './constants'

export const fetchVideoDetails = (id) => ({
    type: FETCH_VIDEO_DETAILS,
    payload: id
})

export const fetchComments = (id) => ({
    type: FETCH_COMMENTS,
    payload: id
})

export const cleanVideoDetails = () => ({
    type: CLEAN_VIDEO
})