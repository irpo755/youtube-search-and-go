import React from 'react'
import {View} from 'react-native'
import {Chip} from 'react-native-paper'
import {kFormatter} from 'app/utils'
import style from 'app/styles'

const VideoStats = ({stats}) => {
    const {viewCount, likeCount, dislikeCount } = stats ? stats : {viewCount: '', likeCount: '', dislikeCount: ''}
    return (
        <View style={style.rowReverseContainer}>
            <Chip style={style.chipStyle} icon="thumb-down">{kFormatter(dislikeCount)}</Chip>
            <Chip style={style.chipStyle} icon="thumb-up">{kFormatter(likeCount)}</Chip>
            <Chip style={style.chipStyle}>{kFormatter(viewCount) + ' views'}</Chip>
        </View>
    )
    
}

export default VideoStats