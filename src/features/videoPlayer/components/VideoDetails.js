import React from 'react'
import { Card, Title, Paragraph } from 'react-native-paper'
import VideoStats from './VideoStats'

const VideoDetails = ({ description, title, statistics}) => (
    <Card >
       <Card.Content>
            <Title>{title}</Title>
            <VideoStats stats={statistics}/>
           <Paragraph>{ description }</Paragraph>
       </Card.Content>
    </Card>
)

export default VideoDetails