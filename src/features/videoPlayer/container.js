import { connect } from 'react-redux'
import {YOUTUBE_API_KEY} from 'app/api/constants'
import { fetchVideoDetails, cleanVideoDetails, fetchComments } from './actions'
import VideoPlayerScreen from './components/VideoPlayerScreen'

const mapStateToProps = (state) => ({
    video: state.SearchVideos.selectedVideo,
    videoDetails: state.VideoPlayer.video,
    isFetching: state.VideoPlayer.isFetching,
    API_KEY: YOUTUBE_API_KEY,
    comments: state.VideoPlayer.comments
})

const mapDispatchToProps = (dispatch) => ({
    fetchVideo: id => { dispatch(fetchVideoDetails(id)) },
    fetchComments: id => { dispatch(fetchComments(id)) },
    cleanVideoDetails: () => { dispatch(cleanVideoDetails()) }
})


export default connect(mapStateToProps,mapDispatchToProps)(VideoPlayerScreen)
