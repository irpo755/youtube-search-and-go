import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import rootReducer from 'app/reducers'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: []
};

// Middleware: Redux Persist Persisted Reducer
export const persistedReducer = persistReducer(persistConfig, rootReducer);
