import { all } from 'redux-saga/effects'
import searchVideosSaga from 'app/features/searchVideos/sagas'
import videoPlayerSaga from 'app/features/videoPlayer/sagas'

export default function* rootSearchVideosSaga(){
    yield all([
        searchVideosSaga(),
        videoPlayerSaga()
    ])
}