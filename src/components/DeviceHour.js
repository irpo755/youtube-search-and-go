import { format } from 'date-fns'
import React from 'react'
import { Text } from 'react-native'

class DeviceHour extends React.Component {
    state = {
        hour: format(new Date, "hh:mm:ss")
    }

    componentDidMount() {
        this.interval = setInterval(
            () => {
                this.setState({
                    hour: format(new Date, "hh:mm:ss aaaa")
                })
            },
            1000
        )
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        return (
            <Text>{this.state.hour}</Text>
        )
    }
}

export default DeviceHour